## Basic step for node project

1. First clone this project.
2. Run this command **npm install** in your project directory.
3. Add '.env' file in your root project directory.
4. Create database connection either local or cloud.
   * Local:                                                          
     link:https://www.youtube.com/watch?v=1uFY60CESlM&list=PL6gx4Cwl9DGDQ5DrbIl20Zu9hx1IjeVhO
   * Cloud:
     link:https://mlab.com/

5. Copy Below this code and paste to .env file.

   * APP_NAME="Project"                            //Your Project Name.
   * NODE_ENV=development                          //Project Environment.
   * DB_DEV_HOST=@ds237192.mlab.com:37192/project  //Database Host url.
   * DB_DEV_PASSWORD=12346                         //Databse Password.
   * DB_DEV_USERNAME=project                       //Datebase username.
   * PORT=3000                                     //Project Port.
   * BASE_URL = http://localhost:3000/api
   * MAIL_EMAIL = demo@gmail.com                   //Gmail Email Address.
   * MAIL_PASSWORD = 123456                        //Email Address Password.
   * MAIL_FROM_NAME = Claim You                    //Your Project Name.

6. Run  **npm start** of your root directory.
